// Base url
var base = $('base').attr('href');

// Variables
var messageCount = 0;
var maxMessage = 2;


function responsive_filemanager_callback(field_id){
	var url=jQuery('#'+field_id).val();
	var thumbnail = '';

	$.ajax({
		url: base + 'api/admin/post/thumbnail',
		type: 'GET',
		dataType: 'text',
		data: {
			'url': url,
			'type' : 'medium',
		},
		success: function(result){
			thumbnail = result.slice(1, -1);
			$('.img-previewer').html('<img src="' + thumbnail + '" class="img-responsive">');
		}
	})	
}

function before_send() {
    return new PNotify({
        text: "Please Wait",
        type: 'info',
        icon: 'fa fa-spinner fa-spin',
        hide: false,
        buttons: {
            closer: false,
            sticker: false
        },
        styling: 'bootstrap3',
        shadow: false,
        width: "150px"
    });
}

/**
 * Hiện thông điệp đến người dùng
 * @param  {[String]} text  [Nội dung thông điệp]
 * @param  {String} type  [Thể loại thông báo (error info....)]
 * @param  {String} title [Tiếu đề]
 * @param  {String} icon  [Biểu tượng]
 * @param  {Number} delay [Thời gian hiện]
 * @return {[type]}       [description]
 */
function showMessage(text, type="success", delay = 5000,title="Thông báo") {
	if (messageCount<maxMessage){
		messageCount++;
	}
	else{
		messageCount=1;
		PNotify.removeAll();
	}
    var notice = new PNotify({
        title: title,
       	text: text,
        type: type,
        styling: 'bootstrap3',
        delay: delay
    })
}

function getMessages(){
	// Get messages
	if (!sessionStorage.getItem(messageId)){
		if (success){
			showMessage(success);
		}
		if (notice){
			showMessage(notice,"notice");
		}
		if (info){
			showMessage(info,"info");
		}
		if (error){
			showMessage(error,"error");
		}
		sessionStorage.setItem(messageId, true);
	}
}

jQuery(document).ready(function($) {
	// Get messages then display
	getMessages();

	// Init TinyMCE
	if (typeof tinymce !== 'undefined'){
		$('.wysiwyg-editor').tinymce({
			selector: "textarea",
			theme: "modern",
		    plugins: [
		        "advlist autolink link image lists charmap print preview hr anchor pagebreak",
		        "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
		        "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
		    ],
		   	toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
		   	toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
		    image_advtab: true ,
		    relative_urls : false,
			remove_script_host : false,
			convert_urls : true,
		    external_filemanager_path: base + "filemanager/",
		    filemanager_title:"Responsive Filemanager" ,
		    external_plugins: { "filemanager" : base + "filemanager/plugin.min.js"}
		});
	}

	// Select image
	$('.img-select').click(function(event) {
		$('.modal iframe').prop('src', base + 'filemanager/dialog.php?type=1&field_id=imageUrl&fldr=anh');
	});

	$('.form-group .add-tag').click(function(event) {
		var val  =  $('.tag-input').val();
		var tags =  val.split(',');

		for (var i = 0; i < tags.length; i++){
			tags[i] = $.trim(tags[i]);
		}

		$('.form-group .tag-input').val('');

		$('input[name="post_tags"]').val(val);

		var parentDiv = $(this).closest('.form-group');

		$.each(tags, function(index, val) {
			parentDiv.prepend('<input type="hidden" name="post_tags[]" value="' + val + '">')
			var tag = '<span class="tag"><i class="fa fa-tags"></i> ' + val + ' <i class="fa fa-times remove"></i></span>';
			$('.form-group .tags').append(tag);
		});
	});

	$('.tags').on('click', '.tag .remove', function(event) {
		var tag = $(this).closest('.tag');
		$('input[value="' + $.trim(tag.text()) + '"]').remove();
		tag.remove();
	});

});
var update_data = {};

function getTree(){
	$.ajax({
		url: base + 'api/admin/post/category_tree',
		type: 'GET',
		dataType: 'html',
		success: function(result){
			$('#category-tree').html(result);
			$( ".sortable" ).sortable({
		      	placeholder: "ui-state-highlight",
		      	stop: function( event, ui ) {
		      		var current_li = ui.item.closest('li');
		      		$.each(current_li.parent().children(), function(index, item) {
		      			var id = $(item).data('id');
		      			update_data[id] = {
			      			'order'     : $(item).index()
			      		};
		      		});
		      	}
		    });
		    $( ".sortable" ).disableSelection();
		}
	})
}
getTree();

function updateTree(){
	console.log(update_data);
	$.ajax({
		url: base + 'api/admin/post/category_order_update',
		type: 'POST',
		dataType: 'json',
		data: {update_data: JSON.stringify(update_data)},
		success: function(result){
			showMessage(result['message']);
			getTree();
		}
	})
}
$('#save').click(function(event) {
	updateTree();
});


	

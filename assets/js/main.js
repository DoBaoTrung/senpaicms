(function(){
	new WOW().init();
	$('.owl-carousel').owlCarousel({
		items: 1,
	});

	if (typeof zoom === "function"){
		$(".zoom-image").zoom({
			magnify: 0.5
		});
	}

	$("#single-product .related-images img").click(function(event) {
		event.preventDefault();
		$('#single-product .related-images img').removeClass('active');
		$(this).addClass('active');
		var url = $(this).prop('src');
		$('#single-product .image img').prop('src', url);
	});
})()
<?php

class Controller_Admin extends Controller_Base
{
	public $template = 'templates/senpai/layout_master';

	protected $_OPTION = array(
		'css' => array(
			'admin/bootstrap.min.css',
	        'admin/font-awesome.min.css',
	        'admin/nprogress.css',
	        'admin/green.css',
	        'admin/bootstrap-progressbar-3.3.4.min.css',
	        'admin/jqvmap.min.css',
	        'admin/daterangepicker.css',
	        'admin/custom.min.css',
	        'admin/pnotify.css',
	        'admin/admin.css'
		),
		'js' => array(
			'admin/jquery.min.js',
	        'admin/bootstrap.min.js',
	        'admin/fastclick.js',
	        'admin/nprogress.js',
	        'admin/Chart.min.js',
	        'admin/gauge.min.js',
	        'admin/icheck.min.js',
	        'admin/bootstrap-progressbar.min.js',
	        // 'admin/jquery.flot.js',
	        // 'admin/jquery.flot.pie.js',
	        // 'admin/jquery.flot.time.js',
	        // 'admin/jquery.flot.stack.js',
	        // 'admin/jquery.flot.resize.js',
	        // 'admin/jquery.flot.orderBars.js',
	        // 'admin/jquery.flot.spline.min.js',
	        // 'admin/curvedLines.js',
	        'admin/date.js',
	        'admin/moment.min.js',
	        'admin/daterangepicker.js',
	        'admin/custom.min.js',
	        'admin/pnotify.js',
	        'admin/admin.js'
		)
	);

	/********************
	 * Abstract methods *
	 ********************/

	protected function init_webparts(){

	    $this->_WEBPART['topnav'] 		= in_array('topnav',$this->_EXCLUDED_WEBPART) ? null : \View::forge('templates/senpai/_includes/topnav');

	    $this->_WEBPART['sidebar_menu'] = in_array('sidebar_menu',$this->_EXCLUDED_WEBPART) ? null : \View::forge('templates/senpai/_includes/sidebar_menu');

	    $this->_WEBPART['footer'] 		= in_array('footer',$this->_EXCLUDED_WEBPART) ? null : \View::forge('templates/senpai/_includes/footer');

	    $this->template->set_global('_WEBPART',$this->_WEBPART);
	}

	/************************
	 * End abstract methods *
	 ************************/

	public function before()
	{
		parent::before();

		// if (Input::method() == 'POST'){
			// if ( ! \Security::check_token()){
			// 	Session::set_flash('error', 'Vui lòng không gửi lại biểu mẫu');
			// 	Response::redirect(Input::referrer());
			// }
		// }

		if (Request::active()->controller !== 'Controller_Admin' or ! in_array(Request::active()->action, array('login', 'logout')))
		{
			if (Auth::check())
			{
				$admin_group_id = Config::get('auth.driver', 'Simpleauth') == 'Ormauth' ? 6 : 100;
				if ( ! Auth::member($admin_group_id))
				{
					Session::set_flash('error', e('You don\'t have access to the admin panel'));
					Response::redirect('/');
				}
			}
			else
			{
				Response::redirect('admin/login');
			}
		}
	}

	public function action_login()
	{
		// Already logged in
		Auth::check() and Response::redirect('admin');

		$val = Validation::forge();

		if (Input::method() == 'POST')
		{
			$val->add('email', 'Email or Username')
			    ->add_rule('required');
			$val->add('password', 'Password')
			    ->add_rule('required');

			if ($val->run())
			{
				if ( ! Auth::check())
				{
					if (Auth::login(Input::post('email'), Input::post('password')))
					{
						if (!empty(Input::post('remember')))
			            {
			                \Auth::remember_me();
			            }
						// assign the user id that lasted updated this record
						foreach (\Auth::verified() as $driver)
						{
							if (($id = $driver->get_user_id()) !== false)
							{

								// credentials ok, go right in
								$current_user = Model\Auth_User::find($id[1]);
								Session::set_flash('success', e('Welcome, '.$current_user->username));
								Response::redirect('admin');
							}
						}
					}
					else
					{
						$this->template->set_global('login_error', 'Login failed!');
					}
				}
				else
				{
					$this->template->set_global('login_error', 'Already logged in!');
				}
			}
		}

		$this->template = \View::forge('templates/senpai/layout_login');
		$this->template->title = 'Login';
		$this->template->content = View::forge('admin/login', array('val' => $val), false);
	}

	/**
	 * The logout action.
	 *
	 * @access  public
	 * @return  void
	 */
	public function action_logout()
	{
		Auth::dont_remember_me();
		Auth::logout();
		Response::redirect('admin');
	}

	/**
	 * The index action.
	 *
	 * @access  public
	 * @return  void
	 */
	public function action_index()
	{

		$this->template->title = 'Dashboard';
		$this->template->content = View::forge('admin/dashboard');
	}

	public function action_profile(){
		$this->template->title = 'Profile';
		$this->template->content = View::forge('admin/profile');
	}

}

/* End of file admin.php */

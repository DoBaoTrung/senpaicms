<?php
class Controller_Admin_Post extends Controller_Admin
{

	public function before(){
		parent::before();
	}

	public function action_index()
	{
		$data['posts'] = Model_Post::find('all');
		$this->template->title = "Posts";
		$this->template->content = View::forge('admin/post/index', $data, false);

	}

	public function action_view($id = null)
	{
		$data['post'] = Model_Post::find($id);
		$this->template->title = "Post";
		$this->template->content = View::forge('admin/post/view', $data);

	}

	private function get_post_tag($post , $edit = false , $tags = array()){
		if (empty($tags))
			$tags = Input::post('post_tags');

		if ($edit) $post->tags = null;

		if ($tags){
			foreach ($tags as $key => $tag){
				// Check if tag exists
				$tag_slug = Helper::slugify($tag);

				$getTag = Model_Post_Tag::query()->where('tag_slug','=',$tag_slug)->get_one();

				if ($getTag){
					$post->tags[] = $getTag;
				}
				else {
					$post->tags[] = Model_Post_Tag::forge(array(
						'tag_name' => $tag,
						'tag_slug' => Helper::slugify($tag),
					));
				}	
			};
		}
		return $post;
	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Post::validate('create');

			if ($val->run())
			{

				$post = Model_Post::forge(array(
					'title' 		=> Input::post('title'),
					'slug' 			=> Helper::slugify(Input::post('title')),
					'summary' 		=> Input::post('summary'),
					'content' 		=> Input::post('content'),
					'feature_image' => Input::post('feature_image') ?: URI::base(). 'assets/' . 'img/no-img.png',
					'user_id' 		=> Auth::get('id'),
					'order' 		=> Input::post('order'),
				));
			
				$post = $this->get_post_tag($post);
				
				if ($post and $post->save())
				{
					
					Session::set_flash('success', e('Added post #'.$post->id.'.'));

					Response::redirect('admin/post');
				}

				else
				{
					Session::set_flash('error', e('Could not save post.'));
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		
		}
		$this->_INCLUDED_JS[] = 'tinymce/tinymce.min.js';
		$this->_INCLUDED_JS[] = 'tinymce/jquery.tinymce.min.js';
		$this->init_assets();

		$this->template->set_global('post_categories',Model_Post_Category::find('all'));
		$this->template->title = "Posts";
		$this->template->content = View::forge('admin/post/create');

	}

	public function action_edit($id = null)
	{
		$post = Model_Post::find($id);
		$val = Model_Post::validate('edit');

		if ($val->run())
		{
			$post->title 		 	= Input::post('title');
			$post->slug 		 	= Helper::slugify(Input::post('title'));
			$post->summary 		 	= Input::post('summary');
			$post->content 		 	= Input::post('content');
			$post->order 		 	= Input::post('order');
			$post->feature_image 	= Input::post('feature_image');

			$post = $this->get_post_tag($post, true);

			if ($post->save())
			{
				Session::set_flash('success', e('Updated post #' . $id));

				Response::redirect('admin/post');
			}

			else
			{
				Session::set_flash('error', e('Could not update post #' . $id));
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$post->title = $val->validated('title');
				$post->slug = $val->validated('slug');
				$post->summary = $val->validated('summary');
				$post->content = $val->validated('content');
				$post->user_id = $val->validated('user_id');
				$post->order = $val->validated('order');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('post', $post, false);
		}

		$this->_INCLUDED_JS[] = 'tinymce/tinymce.min.js';
		$this->_INCLUDED_JS[] = 'tinymce/jquery.tinymce.min.js';
		$this->init_assets();

		$this->template->set_global('post_categories',Model_Post_Category::find('all'));
		$this->template->title = "Posts";
		$this->template->content = View::forge('admin/post/edit');

	}

	public function action_delete($id = null)
	{
		if ($post = Model_Post::find($id))
		{
			$post->delete();

			Session::set_flash('success', e('Deleted post #'.$id));
		}

		else
		{
			Session::set_flash('error', e('Could not delete post #'.$id));
		}

		Response::redirect('admin/post');

	}

}

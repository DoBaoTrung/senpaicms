<?php
class Controller_Admin_Post_Category extends Controller_Admin
{

	public function before(){
		parent::before();
	}

	public function action_index()
	{
		$data['post_categories'] = Model_Post_Category::find('all');
		$this->template->title = "Chuyên mục tin tức";
		$this->template->content = View::forge('admin/post/category/index',$data);

	}

	public function action_view($id = null)
	{
		$data['post_category'] = Model_Post_Category::find($id);

		$this->template->title = "Post_category";
		$this->template->content = View::forge('admin/post/category/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Post_Category::validate('create');

			if ($val->run())
			{

				$parent_category = Model_Post_Category::find(Input::post('parent_id'));

				$new_category = Model_Post_Category::forge(array(
					'name' 		=> Input::post('name'),
					'slug' 		=> Helper::slugify(Input::post('name')),
					'comment' 	=> Input::post('comment'),
				));

				if ($parent_category)
					$result = $new_category->child($parent_category)->save();
				else {
					$result = $new_category->save();
				};

				if ($result)
				{
					Session::set_flash('success', e('Đã thêm chuyên mục #'.$new_category->id.'.'));

					Response::redirect('admin/post/category');
				}

				else
				{
					Session::set_flash('error', e('Could not save post_category.'));
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->set_global('category_list', Model_Post_Category::get_all_categories());
		$this->template->title = "Post_Categories";
		$this->template->content = View::forge('admin/post/category/create');

	}

	public function action_edit($id = null)
	{
		$post_category = Model_Post_Category::find($id);
		$val = Model_Post_Category::validate('edit');

		if ($val->run())
		{
			$post_category->name = Input::post('name');
			$post_category->slug = Helper::slugify(Input::post('name'));
			$post_category->comment = Input::post('comment');

			if ($parent = Model_Post_Category::find(Input::post('parent_id')))
				if ($post_category->child($parent)->save())
				{
					Session::set_flash('success', e('Updated post_category #' . $id));

					Response::redirect('admin/post/category');
				}

				else
				{
					Session::set_flash('error', e('Could not update post_category #' . $id));
				}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$post_category->name = $val->validated('name');
				$post_category->slug = $val->validated('slug');
				$post_category->comment = $val->validated('comment');
				$post_category->order = $val->validated('order');
				$post_category->parent_id = $val->validated('parent_id');
				$post_category->child_count = $val->validated('child_count');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('post_category', $post_category, false);
		}

		$this->template->set_global('category_list', Model_Post_Category::get_all_categories($id));
		$this->template->title = "Chuyên mục tin tức";
		$this->template->content = View::forge('admin/post/category/edit');

	}

	public function action_delete($id = null)
	{
		if ($post_category = Model_Post_Category::find($id))
		{
			$post_category->delete();

			Session::set_flash('success', e('Deleted post_category #'.$id));
		}

		else
		{
			Session::set_flash('error', e('Could not delete post_category #'.$id));
		}

		Response::redirect('admin/post/category');

	}

	public function action_sort(){
		// $this->_INCLUDED_JS[]  = 'admin/jquery-ui-1.10.3.custom.min.js';
		// $this->_INCLUDED_JS[]  = 'admin/jquery-sortable-min.js';
		// $this->_INCLUDED_JS[]  = 'admin/category/sort.js';
		$this->add_css(array(
			'//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.min.css',
		));
		$this->add_js(array(
			'//code.jquery.com/ui/1.12.1/jquery-ui.js',
			'admin/category/sort.js',
		));
	
		$this->template->set_global('tree_categories', Model_Post_Category::get_tree_categories());
		$this->template->title = "Sắp xếp chuyên mục tin tức";
		$this->template->content = View::forge('admin/post/category/sort', null , false);
	}

}

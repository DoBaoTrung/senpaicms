<?php
class Controller_Admin_Spinner extends Controller_Admin
{

	public function action_index()
	{
		$data['spinners'] = Model_Spinner::find('all');
		$this->template->title = "Spinners";
		$this->template->content = View::forge('admin/spinner/index', $data);

	}

	public function action_view($id = null)
	{
		$data['spinner'] = Model_Spinner::find($id);

		$this->template->title = "Spinner";
		$this->template->content = View::forge('admin/spinner/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Spinner::validate('create');

			if ($val->run())
			{
				$spinner = Model_Spinner::forge(array(
					'name' => Input::post('name'),
					'price' => Input::post('price'),
					'short_description' => Input::post('short_description'),
					'specifications' => Input::post('specifications'),
					'country_id' => Input::post('country_id'),
					'status' => Input::post('status'),
					'sale' => Input::post('sale'),
					'bought_times' => Input::post('bought_times'),
				));

				if ($spinner and $spinner->save())
				{
					Session::set_flash('success', e('Added spinner #'.$spinner->id.'.'));

					Response::redirect('admin/spinner');
				}

				else
				{
					Session::set_flash('error', e('Could not save spinner.'));
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Spinners";
		$this->template->content = View::forge('admin/spinner/create');

	}

	public function action_edit($id = null)
	{
		$spinner = Model_Spinner::find($id);
		$val = Model_Spinner::validate('edit');

		if ($val->run())
		{
			$spinner->name = Input::post('name');
			$spinner->price = Input::post('price');
			$spinner->short_description = Input::post('short_description');
			$spinner->specifications = Input::post('specifications');
			$spinner->country_id = Input::post('country_id');
			$spinner->status = Input::post('status');
			$spinner->sale = Input::post('sale');
			$spinner->bought_times = Input::post('bought_times');

			if ($spinner->save())
			{
				Session::set_flash('success', e('Updated spinner #' . $id));

				Response::redirect('admin/spinner');
			}

			else
			{
				Session::set_flash('error', e('Could not update spinner #' . $id));
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$spinner->name = $val->validated('name');
				$spinner->price = $val->validated('price');
				$spinner->short_description = $val->validated('short_description');
				$spinner->specifications = $val->validated('specifications');
				$spinner->country_id = $val->validated('country_id');
				$spinner->status = $val->validated('status');
				$spinner->sale = $val->validated('sale');
				$spinner->bought_times = $val->validated('bought_times');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('spinner', $spinner, false);
		}

		$this->template->title = "Spinners";
		$this->template->content = View::forge('admin/spinner/edit');

	}

	public function action_delete($id = null)
	{
		if ($spinner = Model_Spinner::find($id))
		{
			$spinner->delete();

			Session::set_flash('success', e('Deleted spinner #'.$id));
		}

		else
		{
			Session::set_flash('error', e('Could not delete spinner #'.$id));
		}

		Response::redirect('admin/spinner');

	}

}

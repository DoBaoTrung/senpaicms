<?php 
/**
* 
*/
class Controller_Api_Admin_Base extends Controller_Rest
{
	public function before(){
		parent::before();
		if (!Auth::check() || Auth::get('group') < 50)
			return $this->response(array(
	           'message' => 'cặc'
	        ));
	}


	public function get_message($delay = 3000)
    {
    	$response['message']['success'] = Session::get_flash('success');
        $response['message']['notice']  = Session::get_flash('notice');
        $response['message']['error']   = Session::get_flash('error');
        $response['message_delay']   = $delay;

        return $this->response($response);
    }
}
?>
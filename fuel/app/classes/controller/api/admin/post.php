<?php 
/**
* 
*/
class Controller_Api_Admin_Post extends Controller_Api_Admin_Base
{

    private $temp_content = '';

	public function get_posts()
    {
        $posts = Model_Post::query()->get();
        return $this->response();
    }

    public function get_thumbnail(){
    	$url = Input::get('url'); 
    	$type = Input::get('type') ?: 'small';
    	return $this->response(Helper::thumbnail($url, $type));
    }


    private function the_recursive_tree($parents, $level = 0){
        if ($level == 0){
            $this->temp_content .= '<ol class="sortable">';
        }
        // if ($parents)
            foreach ($parents as $key => $parent) {
                if (empty($parent['children'])){
                    $this->temp_content .= '<li class="ui-state-default" data-id="' . $parent['id'] . '">';
                    $this->temp_content .=      '<span>' . $parent['name'] . '</span>';
                    $this->temp_content .= "</li>";
                }
                else {
                    $this->temp_content .= '<li class="ui-state-default" data-id="' . $parent['id'] . '">';
                    $this->temp_content .=    '<span>';
                    $this->temp_content .=         $parent['name'];
                    $this->temp_content .=    '</span>';

                    $this->temp_content .=    '<ol class="sortable">';
                        $this->the_recursive_tree($parent['children'], $level+1);
                    $this->temp_content .=    "</ol>";
                    $this->temp_content .= "</li>";
                }
            }
        if ($level == 0){
            $this->temp_content .= "</ol>";
        }
        return $this->temp_content;
    }   

    /**
     * THE TREE!!!!!!
     * @return [type] [description]
     */
    public function get_category_tree(){
        // clear temp content
        // $this->temp_content = '';

        // get da tree
        $tree = Model_Post_Category::get_tree_categories();

        $content = $this->the_recursive_tree($tree);
        
        // give da tree
        return $this->response($content);
    }

    // public function post_category_update(){
    //     $updated_categories = Security::xss_clean(Input::post('data'));
    //     $updated_categories = json_decode($updated_categories,true);

    //     $result = array();
    //     $message = '';
    //     $successCount = 0;
    //     $failCount = 0;

    //     if ($updated_categories && is_array($updated_categories)){
    //         foreach ($updated_categories as $key => $item) {

    //             $update_node = Model_Post_Category::find($key);

    //             $parent_node = Model_Post_Category::find(isset($item['parent_id']) ? $item['parent_id'] : '' );

    //             if ($update_node && $parent_node){
                    
    //                 $clone_node = Model_Post_Category::forge(array(
    //                     'id'         => $update_node->id,
    //                     'name'       => $update_node->name,
    //                     'slug'       => $update_node->slug,
    //                     'comment'    => $update_node->comment,
    //                     'created_at' => $update_node->created_at,
    //                     'updated_at' => $update_node->updated_at,
    //                 ));

    //                 $update_node->delete();
    //                 if ($clone_node->child($parent_node)->save())
    //                     $successCount++;
    //                 else $failCount++;
    //             }
    //             else $failCount++;
    //         }
    //     }

    //     $message = '- Số chuyên mục cập nhật thành công: ' . $successCount . '<br>';
    //     $message.= '- Số chuyên mục cập nhật thất bại: ' . $failCount ;

    //     return $this->response(array('message' => $message));
    // }

    public function post_category_order_update(){
        $categories = json_decode(Security::xss_clean(Input::post('update_data')),true);
        
        if ($categories)
            foreach ($categories as $key => $item) {
                $category = Model_Post_Category::find($key);
                $category->order = $item['order'];
                try {
                    $category->save();
                } catch (Exception $e) {
                    echo $e;
                }
            }
        return $this->response(array('message' => 'Đã cập nhật!'));
    }
}
?>
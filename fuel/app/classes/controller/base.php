<?php

abstract class Controller_Base extends Controller_Template
{

	protected $_OPTION = array(
		'css' => null,
		'js' => null,
	);
	protected $_WEBPART = array();
	// excluded
	protected $_EXCLUDED_WEBPART = array();


	public function before()
	{
		parent::before();

		// Assign current_user to the instance so controllers can use it
		$this->current_user = Auth::check()
			? (Config::get('auth.driver', 'Simpleauth') == 'Ormauth' ? Model\Auth_User::find_by_username(Auth::get_screen_name()) : Model_User::find_by_username(Auth::get_screen_name()))
			: null;

		// Set a global variable so views can use it
		View::set_global('current_user', $this->current_user);
	}

	private function init_CMS(){
		$this->current_user = null;
		$this->template->set_global('title','Untitled');
		$this->template->set_global('content','No content.');
	}

	protected function add_css($css = array(), $offset_last = null){
		if (!is_array($css)){
			$temp = $css;
			$css = array();
			array_push($css, $temp);
		}
		if (!$offset_last)
			foreach ($css as $key => $item) {
				array_push($this->_OPTION['css'], $item);
			}
		else foreach ($css as $key => $item) {
			array_splice($this->_OPTION['css'],sizeof($this->_OPTION['css']) - $offset_last, 0 , $item);
		}
	}

	protected function remove_css($css = array()){
		foreach ($css as $key => $item) {
			$this->_OPTION['css'] = array_diff($this->_OPTION['css'], $item);
		}
	}

	protected function add_js($js = array(), $offset_last = null){
		if (!is_array($js)){
			$temp = $js;
			$js = array();
			array_push($js, $temp);
		}

		if (!$offset_last)
			foreach ($js as $key => $item) {
				array_push($this->_OPTION['js'], $item);
			}
		else foreach ($js as $key => $item) {
			array_splice($this->_OPTION['js'],sizeof($this->_OPTION['js']) - $offset_last, 0 , $item);
		}
	}

	protected function remove_js($js = array()){
		foreach ($js as $key => $item) {
			$this->_OPTION['js'] = array_diff($this->_OPTION['js'], $item);
		}
	}

	protected function init_assets(){
		$this->template->set_global("_OPTION",array(
			'css' => $this->_OPTION['css'],
			'js'  => $this->_OPTION['js']
		));
	}

	public function after($response)
    {
        $response = parent::after($response); // not needed if you create your own response object

        $this->init_assets();

        return $response; // make sure after() returns the response object
    }

}

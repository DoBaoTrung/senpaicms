<?php

class Controller_Frontend extends Controller_Base
{
	public $template = 'templates/frontend/master';

	protected $_OPTION = array(
		'css' => array(
			'animate.css',
			'bootstrap3-gridv4.min.css',
			'owl.carousel.min.css',
			'owl.theme.default.css',
			'font-awesome.min.css',
			// 'https://fonts.googleapis.com/css?family=Lato:100,400',
	        'styles.css',

		),
		'js' => array(
			'jquery.min.js',
			'bootstrap.min.js',
			'owl.carousel.min.js',
			'wow.min.js',
			'main.js',
		)
	);

	/************************
	 * End abstract methods *
	 ************************/

	public function action_index()
	{
		$this->template->title 		= 'VNOC Spin - Vietnamese Spinner';
		$this->template->content 	= View::forge('frontend/home');
	}

}

/* End of file admin.php */

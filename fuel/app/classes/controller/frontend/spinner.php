<?php

class Controller_Frontend_Spinner extends Controller_Frontend
{
	public function before(){
		parent::before();
	}

	public function action_index(){
		$this->template->title = 'All spinners';
		$this->template->content = View::forge('frontend/spinner/index');
	}

	public function action_view()
	{
		$this->add_js('jquery.zoom.min.js',1);
		$this->template->title = 'Single product';
		$this->template->content = View::forge('frontend/spinner/view');
	}

}

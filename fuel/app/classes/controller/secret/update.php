<?php 
/**
* 
*/
class Controller_Secret_Update extends Controller_Admin
{
	public function action_index(){
		
		if (Input::method()=='POST'){
			if( ! DBUtil::field_exists('posts', array('post_meta','category_id')))
		    {
		        \DBUtil::add_fields('posts', array(
		            'post_meta' => array('type' => 'text', 'null'=> true),
		            'category_id' => array('constraint' => 11, 'type' => 'int', 'null'=> true),
		        ));
		        echo "Added field 'post_meta','category_id' to table 'customers'";
		    }

		    if( ! DBUtil::field_exists('posts', array('feature_image','other_data')))
		    {
		        \DBUtil::add_fields('posts', array(
		            'feature_image' => array('constraint' => 255, 'type' => 'varchar', 'null'=> true),
		            'other_data' => array('type' => 'text', 'null'=> true),
		        ));
		        echo "Added field 'feature_image','other_data' to table 'customers'";
		    }
		    
		    if( ! DBUtil::table_exists('staffs')){
			    \DBUtil::create_table('staffs', array(
					'id' 			=> array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
					'user_id' 		=> array('constraint' => 11, 'type' => 'int'),
					'name' 			=> array('constraint' => 255, 'type' => 'varchar', 'null' => true),
					'address' 		=> array('constraint' => 255, 'type' => 'varchar', 'null' => true),
					'mobile_number' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),
					'bio' 			=> array('type' => 'text', 'null' => true),
					'created_at'	=> array('constraint' => 11, 'type' => 'int', 'null' => true),
					'updated_at' 	=> array('constraint' => 11, 'type' => 'int', 'null' => true),
				), array('id'));
				echo "Added table staffs";
			}
		}

		$this->template->set_global('content',\View::forge('secret/update'));
	}
}
?>
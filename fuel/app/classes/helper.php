<?php 

class Helper
{

	public static function slugify($str){
        $str = trim(mb_strtolower($str));
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        $str = preg_replace('/[^a-z0-9-\s.]/', '', $str);
        $str = preg_replace('/([\s]+)/', '-', $str);
        return $str;
	}

	public static function thumbnail($url, $type = 'sm'){

		switch ($type) {
			case 'small':
				$type = 'sm';
				break;
			case 'medium':
				$type = 'md';
				break;
			case 'large':
				$type = 'lg';
				break;
		}

		// GET image name, return   < /imagename.xxx >
		$path_parts = pathinfo($url);

		if (isset($path_parts['dirname']) && isset($path_parts['filename']) && isset($path_parts['extension']) != "gif"){
			$thumbnail_url = $path_parts['dirname'] . '/_thumbs/' . $path_parts['filename'] . '-' . $type . '.' . $path_parts['extension'];
			return $thumbnail_url;
		}
		return $url;
	} 

	public static function pr($data){
		echo "<pre>";
		print_r($data);
		echo "</pre>";
		exit;
	}

	public static function print_current_date_vi(){
		echo date('j \t\h\á\n\g n \n\ă\m Y',time());
	}

	public static function rand_str($length = 6,$str = ""){
		$characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
		$max = count($characters) - 1;
		for ($i = 0; $i < $length; $i++) {
			$rand = mt_rand(0, $max);
			$str .= $characters[$rand];
		}
		return $str;
		
	}
}
?>
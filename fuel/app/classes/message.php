<?php 

class Message
{
	public static function get_success(){
		$success = Session::get_flash('success');
		$msg = '';
		if (is_array($success) || is_object($success)){
			foreach ($success as $key => $val) {
				$msg .= '- ' . $val . '<br>';
			}
			return $msg;
		}
		return $success;
	}

	public static function get_info(){
		$info = Session::get_flash('info');
		$msg = '';
		if (is_array($info) || is_object($info)){
			foreach ($info as $key => $val) {
				$msg .= '- ' . $val . '<br>';
			}
			return $msg;
		}
		return $info;
	}


	public static function get_error(){
		$error = Session::get_flash('error');
		$msg = '';
		if (is_array($error) || is_object($error)){
			foreach ($error as $key => $val) {
				$msg .= '- ' . $val . '<br>';
			}
			return $msg;
		}
		return $error;
	}

	public static function get_notice(){
		$notice = Session::get_flash('notice');
		$msg = '';
		if (is_array($notice) || is_object($notice)){
			foreach ($notice as $key => $val) {
				$msg .= '- ' . $val . '<br>';
			}
			return $msg;
		}
		return $notice;
	}
}
?>
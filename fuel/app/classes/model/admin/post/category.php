<?php
use Orm\Model;

class Model_Admin_Post_Category extends Model
{
	protected static $_properties = array(
		'id',
		'name',
		'slug',
		'comment',
		'order',
		'parent',
		'child_count',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('name', 'Name', 'required|max_length[255]');
		$val->add_field('slug', 'Slug', 'required|max_length[255]');
		$val->add_field('comment', 'Comment', 'required');
		$val->add_field('order', 'Order', 'required|valid_string[numeric]');
		$val->add_field('parent', 'Parent', 'required|valid_string[numeric]');
		$val->add_field('child_count', 'Child Count', 'required|valid_string[numeric]');

		return $val;
	}

}

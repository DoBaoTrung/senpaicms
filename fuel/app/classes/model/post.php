<?php
class Model_Post extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'title',
		'slug',
		'summary',
		'content',
		'feature_image',
		'post_meta',
		'category_id',
		'user_id',
		'order',
		'other_data',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_belongs_to = array(
		'user' => array(
			'key_from' => 'user_id',
	        'model_to' => 'Model_User',
	        'key_to' => 'id',
	        'cascade_save' => true,
	        'cascade_delete' => false,
		)
	);

	protected static $_many_many = array(
		'tags' => array(
			'key_from' => 'id',
	        'key_through_from' => 'post_id', // column 1 from the table in between, should match a tags.id
	        'table_through' => 'posts_tags', // both models plural without prefix in alphabetical order
	        'key_through_to' => 'tag_id', // column 2 from the table in between, should match a post.id
	        'model_to' => 'Model_Post_Tag',
	        'key_to' => 'id',
	        'cascade_save' => true,
	        'cascade_delete' => false,
		)
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('title', 'Title', 'required|max_length[255]');
		$val->add_field('summary', 'Summary', 'required');
		$val->add_field('content', 'Content', 'required');

		return $val;
	}

}

<?php
class Model_Post_Category extends \Orm\Model_Nestedset
{
	protected static $_properties = array(
		'id',
		'name',
		'slug',
		'comment',
		'order',
		'created_at',
		'updated_at',
		'left_id',
		'right_id',
		'tree_id',

	);

	protected static $_tree = array(
        'left_field'     => 'left_id',		// name of the tree node left index field
        'right_field'    => 'right_id',		// name of the tree node right index field
        'tree_field'     => 'tree_id',		// name of the tree node tree index field
        'title_field'    => 'name',		//  name of the tree node title field
    );

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	/**
	 * Get all categories
	 * @param  int $exclude_id [Category Id which you want to exclude]
	 * @param  array  $option     [Pass options through array keys and values. Ex: $option['select']]
	 * @return [mixed]             [description]
	 */
	public static function get_all_categories( $exclude_id = null , $option = array() ){
		$categories = Model_Post_Category::query();

		if ($exclude_id) 
			$categories->where('id','!=',$exclude_id);

		if (array_key_exists('select', $option)) 
			$categories->select($option['select']);

		if (array_key_exists('order_by', $option)) 
			$categories->order_by($option['order_by']);

		return $categories->get();
	}

	private static function recursive_usort_categories(&$categories){
        usort($categories, function($a, $b) {
            return $a['order'] - $b['order'];
        });

        foreach ($categories as &$category) {
            if (!empty($category['children'])){
                static::recursive_usort_categories($category['children']);
            }
        }
    }

	/**
	 * Get all categories by recursive way
	 * @return [object] [whole recusive tree]
	 */
	public static function get_tree_categories(){
		$post_categories = array();
		$roots = Model_Post_Category::forge()
				->roots()
				->get();
		if ($roots){
			foreach ($roots as $key => $root) {
				$post_categories = $root->dump_tree();
			}
		}
		static::recursive_usort_categories($post_categories);

		return $post_categories;
	}

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('name', 'Name', 'required|max_length[255]');
		return $val;
	}

}

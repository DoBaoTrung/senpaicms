<?php

class Model_Post_Tag extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'tag_name',
		'tag_slug',
		'tag_description',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_many_many = array(
		'posts' => array(
			'key_from' => 'id',
	        'key_through_from' => 'tag_id', // column 1 from the table in between, should match a tags.id
	        'table_through' => 'posts_tags', // both models plural without prefix in alphabetical order
	        'key_through_to' => 'post_id', // column 2 from the table in between, should match a post.id
	        'model_to' => 'Model_Post',
	        'key_to' => 'id',
	        'cascade_save' => true,
	        'cascade_delete' => false,
		)
	);

	protected static $_table_name = 'post_tags';

}

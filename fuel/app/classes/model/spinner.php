<?php
class Model_Spinner extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'name',
		'price',
		'short_description',
		'specifications',
		'country_id',
		'status',
		'sale',
		'bought_times',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('name', 'Name', 'required|max_length[255]');
		$val->add_field('price', 'Price', 'required|valid_string[numeric]');
		$val->add_field('short_description', 'Short Description', 'required');
		$val->add_field('specifications', 'Specifications', 'required');
		$val->add_field('country_id', 'Country Id', 'required');
		$val->add_field('status', 'Status', 'required|valid_string[numeric]');
		$val->add_field('sale', 'Sale', 'required|valid_string[numeric]');
		$val->add_field('bought_times', 'Bought Times', 'required|valid_string[numeric]');

		return $val;
	}

}

<?php
return array(
	'_root_'  => 'frontend/index',  // The default route
	// '_404_'   => 'home/404',    // The main 404 route
	
	'login' 		   => array('admin/login', 'name' => 'login'),
	'logout' 		   => array('admin/logout', 'name' => 'logout'),
	'secret-update'    => array('secret/update/index', 'name' => 'secret_update'),

	// api
	'api/admin/(:segment)/(:any)' => array('api/admin/$1/$2', 'name' => 'api_route'),

	// Admin
	'admin/dashboard' 	=> array('admin/index', 'name' => 'dashboard'),
	'admin/profile'     => array('admin/profile', 'name' => 'admin_profile'),

	// Frontend
	'spinner'			=> array('frontend/spinner/index', 'name' => 'frontend_spinner'),
	'spinner/(:slug)'	=> array('frontend/spinner/view', 'name' => 'frontend_spinner_view'),
);

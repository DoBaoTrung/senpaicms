<?php

return array(
	'required'        => 'Chưa nhập :label.',
	'min_length'      => ':label phải chứa ít nhất :param:1 ký tự.',
	'max_length'      => ':label chỉ được chứa tối đa :param:1 ký tự.',
	'exact_length'    => ':label phải chứa chính xác :param:1 ký tự.',
	'match_value'     => ':label phải chứa từ khoá :param:1.',
	'match_pattern'   => ':label phải khớp định dạng :param:1.',
	'match_field'     => ':label phải khớp với trường :param:1.',
	'valid_email'     => ':label phải là email hợp lệ.',
	'valid_emails'    => ':label phải chứa danh sách email hợp lệ.',
	'valid_url'       => ':label phải là URL hợp lệ.',
	'valid_ip'        => ':label phải là địa chỉ IP hợp lệ.',
	'numeric_min'     => 'Số nhỏ nhất của :label là :param:1',
	'numeric_max'     => 'Số lớn nhất của :label là :param:1',
	'numeric_between' => 'Giá trị của :label phải nằm trong khoảng :param:1 đến :param:2',
	'valid_string'    => 'The valid string rule :rule(:param:1) failed for field :label',
	'required_with'   => 'The field :label must contain a value if :param:1 contains a value.',
	'valid_date'      => ':label chứa ngày tháng không hợp lệ.',
);

<?php

namespace Fuel\Migrations;

class Create_spinners
{
	public function up()
	{
		\DBUtil::create_table('spinners', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'name' => array('constraint' => 255, 'type' => 'varchar'),
			'price' => array('constraint' => 11, 'type' => 'int'),
			'short_description' => array('type' => 'text', 'null' => true),
			'specifications' => array('type' => 'null'),
			'country_id' => array('type' => 'null'),
			'status' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'sale' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'bought_times' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('spinners');
	}
}
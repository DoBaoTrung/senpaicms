<?php echo Form::open(array("class"=>"form-horizontal")); ?>
	<input type="hidden" name="feature_image" id="imageUrl" value="<?php echo isset($post) ? $post->feature_image : '' ?>">
	
	<?php if (isset($post->tags)): ?>
		<?php foreach ($post->tags as $key => $tag): ?>
			<input type="hidden" name="post_tags[]" value="<?php echo $tag->tag_name ?>">
		<?php endforeach ?>
	<?php endif ?>


	<div class="modal fade" id="selectModal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Thư viện file</h4>
				</div>
				<div class="modal-body">
					<iframe width="100%" height="500px" src="" frameborder="0"></iframe>
				</div>
			</div>
		</div>
	</div>

	<div class="row gutter-30">
		
		<fieldset class="col-xs-12 col-sm-8">
			<div class="form-group">
				<?php echo Form::label('Title', 'title', array('class'=>'control-label')); ?>

					<?php echo Form::input('title', Input::post('title', isset($post) ? $post->title : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Title')); ?>

			</div>

			<div class="form-group">
				<?php echo Form::label('Summary', 'summary', array('class'=>'control-label')); ?>

					<?php echo Form::textarea('summary', Input::post('summary', isset($post) ? $post->summary : ''), array('class' => 'col-md-8 form-control', 'rows' => 8, 'placeholder'=>'Summary')); ?>

			</div>
			<div class="form-group">
				<?php echo Form::label('Content', 'content', array('class'=>'control-label')); ?>

					<?php echo Form::textarea('content', Input::post('content', isset($post) ? $post->content : ''), array('class' => 'col-md-8 form-control wysiwyg-editor', 'rows' => 8, 'placeholder'=>'Content')); ?>

			</div>

			<div class="form-group">
				<?php echo Form::label('Order', 'order', array('class'=>'control-label')); ?>

					<?php echo Form::input('order', Input::post('order', isset($post) ? $post->order : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Order', 'type' => 'number')); ?>

			</div>

			<div class="form-group">
				<!-- <input type="hidden" name="post_tags[]"> -->
				

				<label for="post_tags" class="control-label">Tag bài viết</label>
				<div class="input-group">
				   <input type="text" class="form-control tag-input" data-toggle="tooltip" placeholder="Thêm tag cho bài viết" title="Mỗi tag ngăn cách nhau bằng dấu phẩy">
				   <span class="input-group-btn">
				        <button class="btn btn-default add-tag" type="button">Thêm!</button>
				   </span>
				</div>
				<p class="tags">
				<?php if (isset($post->tags)): ?>
					<?php foreach ($post->tags as $key => $tag): ?>
						<span class="tag">
							<i class="fa fa-tags"></i> <?php echo $tag->tag_name?> <i class="fa fa-times remove"></i></span>
						</span>
					<?php endforeach ?>
					
				<?php endif ?>
					
				</p>
			</div>

		</fieldset>

		<div class="col-xs-12 col-sm-4">
			<div class="action-container">
				<div class="title-box bg-blue">
					<p class="title">Thao tác </p>
				</div>
				<div class="action-box text-right">
					<div class="info">
						<p>
							<i class="fa fa-eye"> </i>
							<strong> Mức độ công khai:</strong> Công chúng
						</p>
						<p>
							<i class="fa fa-calendar"> </i>
							<strong> Chế độ đăng:</strong> Ngay lập tức
						</p>
					</div>

					<div class="actions">
						<button name="save" value="save" class="post btn btn-sm color-inverse bg-blue">
							Submit
						</button>
					</div>
				</div>
			</div>

			<div class="action-container">
				<div class="title-box bg-orange">
					<p class="title">Ảnh đại diện</p>
				</div>
				<div class="content-box">
					<div class="action-box text-right">
						<div class="info">
							<p>
								<a href="#" data-toggle="modal" data-target="#selectModal" name="action" value="add" class="img-select img-previewer">
									<?php if (isset($post)) :?>
										<img src="<?php echo Helper::thumbnail($post->feature_image, 'medium') ?>" alt="post image" class="center-block img-responsive">
										<?php else: ?>
											<?php echo Asset::img('no-img.png', array('class' => 'center-block img-responsive')) ?>
									<?php endif; ?>
								</a>
							</p>

						</div>
						
						<div class="actions">
							<a href="#" data-toggle="modal" data-target="#selectModal"name="action" value="add" class="img-select btn btn-sm color-inverse bg-orange">Chọn ảnh</a>
						</div>
					</div>
				</div>
			</div>

			<div class="action-container">
				<div class="title-box bg-blue">
					<p class="title">Chuyên mục</p>
				</div>
				<div class="action-box">
					<ul class="category-list">
						<?php foreach ($post_categories as $key => $post_category): ?>
							<li>
								<input type="checkbox" name="post_category[]" value="<?php echo $post_category->id ?>" class="senpai-cb">
								<span class="senpai-label"><?php echo $post_category->name?></span>
							</li>
						<?php endforeach ?>
					</ul>
				</div>
			</div>

		</div>
	</div>
<?php echo Form::close(); ?>
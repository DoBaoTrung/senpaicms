<?php echo Form::open(array("class"=>"form-horizontal")); ?>

	<fieldset>
		<div class="form-group">
			<?php echo Form::label('Name', 'name', array('class'=>'control-label')); ?>

				<?php echo Form::input('name', Input::post('name', isset($post_category) ? $post_category->name : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Name')); ?>

		</div>

		<div class="form-group">
			<?php echo Form::label('Chuyên mục cha', 'parent_id', array('class'=>'control-label')); ?>
			
			<select name="parent_id" class="form-control">
				<?php foreach ($category_list as $key => $item): ?>
					<option value="<?php echo $item->id?>"> <?php echo $item->name ?> </option>
				<?php endforeach ?>
			</select>
			
		</div>

		<div class="form-group">
			<?php echo Form::label('Ghi chú', 'comment', array('class'=>'control-label')); ?>

				<?php echo Form::textarea('comment', Input::post('comment', isset($post_category) ? $post_category->comment : ''), array('class' => 'col-md-8 form-control', 'rows' => 8, 'placeholder'=>'Comment')); ?>

		</div>

		<div class="form-group">
			<?php echo Form::submit('submit', 'Save', array('class' => 'btn btn-success')); ?>		</div>
	</fieldset>
<?php echo Form::close(); ?>
<h2>Listing Post_categories</h2>
<br>
<?php if ($post_categories): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Name</th>
			<th>Slug</th>
			<th>Comment</th>
			<th>Parent</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($post_categories as $item): ?>		<tr>

			<td><?php echo $item->name; ?></td>
			<td><?php echo $item->slug; ?></td>
			<td><?php echo $item->comment; ?></td>
			<td>
				<?php echo !empty($parent = $item->parent()->get_one()) ? $parent->name : 'Uncategorized'  ?>
			</td>

			<td>
				<?php echo Html::anchor('admin/post/category/view/'.$item->id, 'View'); ?> |
				<?php echo Html::anchor('admin/post/category/edit/'.$item->id, 'Edit'); ?> |
				<?php echo Html::anchor('admin/post/category/delete/'.$item->id, 'Delete', array('onclick' => "return confirm('Are you sure?')")); ?>

			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>No Post_categories.</p>

<?php endif; ?><p>
	<?php echo Html::anchor('admin/post/category/create', 'Add new Post category', array('class' => 'btn btn-success')); ?>

</p>

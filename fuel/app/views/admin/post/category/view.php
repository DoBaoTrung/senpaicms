<h2>Viewing #<?php echo $post_category->id; ?></h2>

<p>
	<strong>Name:</strong>
	<?php echo $post_category->name; ?></p>
<p>
	<strong>Slug:</strong>
	<?php echo $post_category->slug; ?></p>
<p>
	<strong>Comment:</strong>
	<?php echo $post_category->comment; ?></p>
<p>
	<strong>Order:</strong>
	<?php echo $post_category->order; ?></p>
<p>
	<strong>Parent:</strong>
	<?php echo $post_category->parent; ?></p>
<p>
	<strong>Child count:</strong>
	<?php echo $post_category->child_count; ?></p>

<?php echo Html::anchor('admin/post/category/edit/'.$post_category->id, 'Edit'); ?> |
<?php echo Html::anchor('admin/post/category', 'Back'); ?>
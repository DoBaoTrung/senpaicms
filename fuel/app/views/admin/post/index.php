<h2>Listing Posts</h2>
<br>

<?php if ($posts): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Title</th>
			<th>Slug</th>
			<th>Summary</th>
			<th>Content</th>
			<th>User id</th>
			<th>Order</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($posts as $item): ?>		<tr>

			<td><?php echo $item->title; ?></td>
			<td><?php echo $item->slug; ?></td>
			<td><?php echo $item->summary; ?></td>
			<td><?php echo $item->content; ?></td>
			<td><?php echo $item->user->username; ?></td>
			<td><?php echo $item->order; ?></td>
			<td>
				<?php echo Html::anchor('admin/post/view/'.$item->id, 'View'); ?> |
				<?php echo Html::anchor('admin/post/edit/'.$item->id, 'Edit'); ?> |
				<?php echo Html::anchor('admin/post/delete/'.$item->id, 'Delete', array('onclick' => "return confirm('Are you sure?')")); ?>

			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>No Posts.</p>

<?php endif; ?><p>
	<?php echo Html::anchor('admin/post/create', 'Add new Post', array('class' => 'btn btn-success')); ?>

</p>

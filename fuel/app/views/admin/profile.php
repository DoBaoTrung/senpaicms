<div class="profile-page">
		<div class="left-col">
			<?php echo Asset::img('default-user.png', array('class' => 'center-block profile-image')) ?>
			<div class="profile-actions form-group">
				<button class="btn btn-info avatar-change"><i class="fa fa-image"></i> Đổi ảnh đại diện</button>
				<button class="btn btn-danger password-change"><i class="fa fa-key"></i> Đổi mật khẩu</button>
			</div>
		</div>

		<div class="right-col">
			<form action="" method="POST" role="form">
				<legend>Trang cá nhân</legend>
					
				<div class="form-group">
					<label for="">Họ và tên</label>
					<input type="text" class="form-control" id="" placeholder="Input field">
				</div>

				<div class="form-group">
					<label for="">Số điện thoại</label>
					<input type="text" class="form-control" id="" placeholder="Input field">
				</div>

				<div class="form-group">
					<label for="">Địa chỉ</label>
					<input type="text" class="form-control" id="" placeholder="Input field">
				</div>
			
				<button type="submit" class="btn btn-success">Submit</button>
			</form>
		</div>
	</div>
</div>

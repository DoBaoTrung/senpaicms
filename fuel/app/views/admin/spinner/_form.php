<?php echo Form::open(array("class"=>"form-horizontal")); ?>

	<fieldset>
		<div class="form-group">
			<?php echo Form::label('Name', 'name', array('class'=>'control-label')); ?>

				<?php echo Form::input('name', Input::post('name', isset($spinner) ? $spinner->name : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Name')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Price', 'price', array('class'=>'control-label')); ?>

				<?php echo Form::input('price', Input::post('price', isset($spinner) ? $spinner->price : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Price')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Short description', 'short_description', array('class'=>'control-label')); ?>

				<?php echo Form::textarea('short_description', Input::post('short_description', isset($spinner) ? $spinner->short_description : ''), array('class' => 'col-md-8 form-control', 'rows' => 8, 'placeholder'=>'Short description')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Specifications', 'specifications', array('class'=>'control-label')); ?>

				<?php echo Form::input('specifications', Input::post('specifications', isset($spinner) ? $spinner->specifications : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Specifications')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Country id', 'country_id', array('class'=>'control-label')); ?>

				<?php echo Form::input('country_id', Input::post('country_id', isset($spinner) ? $spinner->country_id : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Country id')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Status', 'status', array('class'=>'control-label')); ?>

				<?php echo Form::input('status', Input::post('status', isset($spinner) ? $spinner->status : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Status')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Sale', 'sale', array('class'=>'control-label')); ?>

				<?php echo Form::input('sale', Input::post('sale', isset($spinner) ? $spinner->sale : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Sale')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Bought times', 'bought_times', array('class'=>'control-label')); ?>

				<?php echo Form::input('bought_times', Input::post('bought_times', isset($spinner) ? $spinner->bought_times : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Bought times')); ?>

		</div>
		<div class="form-group">
			<label class='control-label'>&nbsp;</label>
			<?php echo Form::submit('submit', 'Save', array('class' => 'btn btn-primary')); ?>		</div>
	</fieldset>
<?php echo Form::close(); ?>
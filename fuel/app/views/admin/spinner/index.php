<h2>Listing Spinners</h2>
<br>
<?php if ($spinners): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Name</th>
			<th>Price</th>
			<th>Short description</th>
			<th>Specifications</th>
			<th>Country id</th>
			<th>Status</th>
			<th>Sale</th>
			<th>Bought times</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($spinners as $item): ?>		<tr>

			<td><?php echo $item->name; ?></td>
			<td><?php echo $item->price; ?></td>
			<td><?php echo $item->short_description; ?></td>
			<td><?php echo $item->specifications; ?></td>
			<td><?php echo $item->country_id; ?></td>
			<td><?php echo $item->status; ?></td>
			<td><?php echo $item->sale; ?></td>
			<td><?php echo $item->bought_times; ?></td>
			<td>
				<?php echo Html::anchor('admin/spinner/view/'.$item->id, 'View'); ?> |
				<?php echo Html::anchor('admin/spinner/edit/'.$item->id, 'Edit'); ?> |
				<?php echo Html::anchor('admin/spinner/delete/'.$item->id, 'Delete', array('onclick' => "return confirm('Are you sure?')")); ?>

			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>No Spinners.</p>

<?php endif; ?><p>
	<?php echo Html::anchor('admin/spinner/create', 'Add new Spinner', array('class' => 'btn btn-success')); ?>

</p>

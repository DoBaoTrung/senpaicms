<h2>Viewing #<?php echo $spinner->id; ?></h2>

<p>
	<strong>Name:</strong>
	<?php echo $spinner->name; ?></p>
<p>
	<strong>Price:</strong>
	<?php echo $spinner->price; ?></p>
<p>
	<strong>Short description:</strong>
	<?php echo $spinner->short_description; ?></p>
<p>
	<strong>Specifications:</strong>
	<?php echo $spinner->specifications; ?></p>
<p>
	<strong>Country id:</strong>
	<?php echo $spinner->country_id; ?></p>
<p>
	<strong>Status:</strong>
	<?php echo $spinner->status; ?></p>
<p>
	<strong>Sale:</strong>
	<?php echo $spinner->sale; ?></p>
<p>
	<strong>Bought times:</strong>
	<?php echo $spinner->bought_times; ?></p>

<?php echo Html::anchor('admin/spinner/edit/'.$spinner->id, 'Edit'); ?> |
<?php echo Html::anchor('admin/spinner', 'Back'); ?>

<!-- Start #intro -->
<div id="intro" class="owl-carousel">
	<div class="slide">
		<h2 class="title"><a href="">Eagle &amp; Smoke</a></h2>
		<?php echo Asset::img('bg2.jpg', array('class' => 'img-responsive')); ?>
		<p class="description text-center">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur eaque vel, inventore soluta molestiae! Nobis corporis perferendis quam, beatae sunt!
		</p>
	</div>
</div>
<!-- End #intro -->

<!-- Start #featured-spinners -->
<div id="featured-spinners">
	<div class="title-group bg-red">
		<h2>Featured Spinners</h2>
	</div>
	<div class="product-wrapper">
		<div class="row wow fadeInUp" data-wow-delay="0.3s">
			<?php for ($i=0;$i<3;$i++): ?>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<div class="product">
						<div class="image">
							<div class="inner">
								<?php echo Asset::img('spinner.jpg', array('class' => 'img-responsive center-block')) ?>
							</div>
						</div>
						<div class="content">
							<h3 class="name"><a href="">Camo Fidget Spinner</a></h3>
							<p class="price">200$</p>
							<p class="description">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione, repudiandae.
							</p>
						</div>
						<button class="add-to-cart">
							<i class="fa fa-cart-plus"></i><span>Add to cart</span>
						</button>
					</div>
				</div>
			<?php endfor; ?>
		</div>
	
		<a href="<?php echo Router::get('frontend_spinner_view') ?>" class="d-ib view-all wow fadeIn" data-wow-delay="0.7s">View All</a>
	</div>
</div>
<!-- End #featured-spinners -->

<!-- Start #featured-accessories -->
<div id="featured-accessories">
	<div class="title-group bg-grey">
		<h2>Featured Accessories</h2>
	</div>
	<div class="product-wrapper">
		<div class="row wow fadeInUp" data-wow-delay="0.3s">
			<?php for ($i=0;$i<3;$i++): ?>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<div class="product">
						<div class="image">
							<div class="inner">
								<?php echo Asset::img('accessories.jpg', array('class' => 'img-responsive center-block')) ?>
							</div>
						</div>
						<div class="content">
							<h3 class="name"><a href="">Accessories #<?= $i+1?></a></h3>
							<p class="price">200$</p>
							<p class="description">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione, repudiandae.
							</p>
						</div>
						<button class="add-to-cart">
							<i class="fa fa-cart-plus"></i><span>Add to cart</span>
						</button>
					</div>
				</div>
			<?php endfor; ?>
		</div>
		<a href="<?php echo Router::get('frontend_spinner_view') ?>" class="d-ib view-all wow fadeIn" data-wow-delay="0.7s">View All</a>
	</div>
</div>
<!-- End #featured-accessories -->

<!-- Start #featured-spinners -->
<div id="featured-spinners">
	<div class="title-group bg-red">
		<h2>Featured Spinners</h2>
	</div>
	<div class="product-wrapper">
		<div class="row wow fadeInUp" data-wow-delay="0.3s">
			<?php for ($i=0;$i<6;$i++): ?>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<div class="product">
						<div class="image">
							<div class="inner">
								<?php echo Asset::img('spinner.jpg', array('class' => 'img-responsive center-block')) ?>
							</div>
						</div>
						<div class="content">
							<h3 class="name"><a href="">Camo Fidget Spinner</a></h3>
							<p class="price">200$</p>
							<p class="description">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione, repudiandae.
							</p>
						</div>
						<button class="add-to-cart">
							<i class="fa fa-cart-plus"></i><span>Add to cart</span>
						</button>
					</div>
				</div>
			<?php endfor; ?>
		</div>
	
		<nav class="product-nav">
			<ul>
				<li class="d-ib"><a href="" class="btn color-white">1</a></li>
				<li class="d-ib"><a href="" class="btn color-white">2</a></li>
				<li class="d-ib"><a href="" class="btn color-white">3</a></li>
			</ul>
		</nav>
	</div>
</div>
<!-- End #featured-spinners -->
<div id="single-product">

	<div class="row">
		<div class="col-xs-12 col-md-5">
			<div class="image">
				<div class="inner zoom-image">
					<img src="<?= URI::base()?>assets/img/accessories.jpg" width="100%" alt="" class="img-responsive">
				</div>
			</div>
			<div class="related-images">
				<div class="row auto-clear gutter-15">
					<div class="col-xs-6 col-sm-4">
						<img src="<?= URI::base()?>assets/img/accessories.jpg" alt="" class="img-responsive active">
					</div>
					<div class="col-xs-6 col-sm-4">
						<img src="<?= URI::base()?>assets/img/spinner.jpg" alt="" class="img-responsive">
					</div>
					<div class="col-xs-6 col-sm-4">
						<img src="<?= URI::base()?>assets/img/spinner.jpg" alt="" class="img-responsive">
					</div>
				</div>
			</div>
			<div class="text-center">
				<a href="" class="back">
					<i class="fa fa-arrow-left"></i>Browsing other spinners
				</a>
			</div>
		</div>

		<div class="col-xs-12 col-md-7">
			<div class="product-info">
				<h1 class="product-name">FUCKING VEGETABO SPINNER</h1>
				<p class="price">$200.00</p>

				<button class="add-to-cart">Add to cart</button>

				<p class="short-description">
					Write some short description here.
				</p>

				<div class="specifiments">
					<strong class="title">SPECIFICATIONS</strong>
					<p><b>Dimensions:</b>
					<br>2.2” (56mm) Length x 1.6” (41mm) Width<br>.5” (12.7mm) Tall at the Buttons</p>
					<p><b>Weight:</b><br>Approx. 2.8 oz (80 grams)</p>
					<p><b>Material:</b><br>303 Stainless Steel Body and Buttons</p>
					<p><b>Finish:</b><br>Tumbled with Ceramic Polishing Media to create&nbsp;a machine finish with a silky smooth feel</p>
					<p><b>Bearings:</b><br>Ceramic/SS Hybrid R188 10 Ball <br>(One Drop YoYo) SS R188 10 Ball</p>
					<p>Certificate of Authenticity Card will have a Serial Number</p>
					<p>Proudly Designed and Manfactured in USA&nbsp;&nbsp;</p>
				</div>
			</div>
		</div>
	</div>

</div>
<!-- Start aside#sidebar -->
<div id="sidebar">
	<!-- Start #logo -->
	<div id="logo">
		<div class="inner">
			<a href="<?php echo URI::base()?>">
				<?php echo Asset::img('logo.png', array('class' => 'img-responsive center-block')) ?>
			</a>
		</div>
	</div>
	<!-- End #logo -->
	<!-- Start #menu -->
	<nav id="menu">
		<ul>
			<li><a href="<?php echo URI::base()?>">Home</a></li>
			<li><a href="<?php echo Router::get('frontend_spinner')?>">Products</a></li>
			<li><a href="">Accessories</a></li>
			<li><a href="">Contact</a></li>
			<li><a href="">About us</a></li>
		</ul>
	</nav>
	<!-- End #menu -->
	<div id="copyright">
		<small>© 2017 VNOC.COM - ALL RIGHTS RESERVED</small>
	</div>
</div>
<!-- End #sidebar -->
<!DOCTYPE html>
<html ng-app="app">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo isset($title) ? $title : 'Untitled' ?></title>
	<?php echo Asset::css($_OPTION['css'])?>
</head>
<body>
	<?php echo render('templates/frontend/_includes/sidebar')?>
	
	<!-- Start #main-content -->
	<div id="main-content" role="main">
		<?php echo $content ?>
	</div>
	<!-- End #main-content -->
	<?php echo Asset::js($_OPTION['js'])?>
</body>
</html>
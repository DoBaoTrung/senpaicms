
<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title bg-green" style="border: 0;">
            <a href="<?php echo URI::base()?>" class="site_title">
                <i class="fa fa-key"></i> 
                <span>Senpai CMS</span>
            </a>
        </div>
        <div class="clearfix"></div>
        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <div class="profile_pic">
               <?php echo Asset::img('default-user.png',array('class' => 'img-circle profile_img', 'alt' => 'Profile Image')) ?>
            </div>
            <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo $current_user->username ?></h2>
            </div>
        </div>
        <!-- /menu profile quick info -->
        <br>

        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                    <li>
                        <a href="<?php echo Router::get('dashboard') ?>"><i class="fa fa-home"></i> Home </a>
                       
                    </li>
                    <li><a><i class="fa fa-edit"></i> Quản lý bài viết <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?php echo Router::get('admin_post') ?>">Danh sách bài viết</a></li>
                            <li><a href="<?php echo Router::get('admin_post_category') ?>">Chuyên mục bài viết</a></li>
                            <li><a href="<?php echo Router::get('admin_post_category_sort') ?>">Sắp xếp chuyên mục</a></li>
                        </ul>
                    </li>
                    <li>  
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true">&nbsp;</span>
                            Settings
                        </a>
                    </li>
                </ul>
            </div>   
        </div>

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>
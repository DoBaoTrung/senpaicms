<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Batman login form</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
    <?php echo Asset::css('admin/login.css') ?>
<body>
    <div id="form-outer">
        <div class="inner">
            <form action="" method="POST">
                <?php echo \Form::csrf() ?>
                <h1>Login</h1>
                <div class="inset">
                    <p>
                        <label for="email">Email/username</label>
                        <input type="text" name="email" id="email">
                    </p>
                    <p>
                        <label for="password">Password</label>
                        <input type="password" name="password" id="password">
                    </p>
                    <p>
                        <input type="checkbox" name="remember" id="remember">
                        <label for="remember">Remember meh</label>
                    </p>
                </div>
                <p class="p-container">
                    <span>Forgot password ?</span>
                    <input type="submit" name="go" id="go" value="Log in">
                </p>
            </form>
        </div>  
    </div>
</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="<?php echo URI::base()?>">
    <title><?php echo $title ?></title>
    <?php echo Asset::css($_OPTION['css'])?>
    
</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <!-- sidebar menu -->
            <?= render('templates/senpai/_includes/sidebar_menu') ?>
            <!-- /sidebar menu -->

            <!-- top navigation -->
            <?= render('templates/senpai/_includes/topnav') ?>
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">
                <div class="page-title">
                    <div class="title_left">
                        <h3><?php echo isset($title) ? $title : '' ?></h3>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <?php echo $content ?>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /page content -->

            <!-- footer content -->
            <?= render('templates/senpai/_includes/footer')?>
            <!-- /footer content -->
        </div>
    </div>
    <script id="app-variables">
        var success     = "<?php echo Message::get_success()?>";
        var error       = "<?php echo Message::get_error()?>";
        var info        = "<?php echo Message::get_info()?>";
        var notice      = "<?php echo Message::get_notice()?>";
        var messageId   = '<?php echo Helper::rand_str(30)?>';
    </script>
    <?php echo Asset::js($_OPTION['js']) ?>
</body>

</html>
